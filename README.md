# Piveau and Data Interface Provider (DPI)

This documentation is an extension of the main setup of Piveau. We forked the main repo and in here we documented our steps to setup Piveau and DPI. 


# Setup of Keycloak

The setup of the Keycloak is based on the main [documentation](https://doc.piveau.eu/guides/integration-keycloak/). We added the "postgresql" and "keycloack" docker services as [suggested](https://doc.piveau.eu/guides/integration-keycloak/#keycloak-deployment). These services were combined in the main "[docker-compose-hub.yml](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/quick-start/docker-compose-hub.yml?ref_type=heads)" so that all the services are running under the same network. 

## Keycloak configuration

Although the realm file for Keycloak is provided [here](https://doc.piveau.eu/guides/integration-keycloak/#upload), we adapted our realm where the CORS setting is also available. Our realm file can be found [here](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/realm-export.json?ref_type=heads). For more details on these adaptations, check the [CHANGELOG.md](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/CHANGELOG.md) file.
> **Note:** our Keycloak is running in port **8093** in the docker-compose file.



## Service Configuration
We adapted the Keycloak configuration in the environment variables of **piveau-hub-repo** and **piveau-hub-ui** as mentioned in the following:
- **piveau-hub-repo:** We needed to adjust the "PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA" environment variable as suggested [here](https://doc.piveau.eu/guides/integration-keycloak/#hub-repo-configuration). However, we added "issuer" as well in the configuration. Please see the [docker-compose-hub.yml](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/quick-start/docker-compose-hub.yml?ref_type=heads) for local setup for us. 
> **Note**: Few important points
> - for the ''issuer" URL, the keycloak URL should beto the publicly available keycloak URL. The one you can access via the browser. 
> - get the client secret from Keyclock running in port http://localhost:8093 . 
> - for "serverUrl" in the configuration, it should be the name of the Keyclock service in the docker file and the docker service's port where it is running. For us it was "http://keycloak:8080/".
> - Do not forget to add forward slash "/" at the end of the serverUrl.
- **piveau-hub-ui:** Add the local setup's URL for the running Keycloak. For us, it was "http://localhost:8093/".

## Setup Piveau
After all the configurations of the docker file were complete, we ran them to have the local instance of **Pivuea**. We followed the provided commands provided in this [quick start](https://doc.piveau.de/general/quick-start/).

## Accessing Data Interface Provider (DPI)
If we login to the Piveau to access DPI. The user created in Keycloak will be the user credentials to do so. 

## Hand-on
In this section, we first created our own catalog, then created user and group and added the user to the group in the DPI. Finally we attempted to create our own dataset.
- **Creating own catalog:** Once Piveau is running the "piveau hub-repo service" on port 8081 of localhost which looks like the image below: ![1](/uploads/a2a2da4d4c2594bcf8535a4891a07b2d/1.png)
We sent a Postman request to the port. We have to set the API key in the request to be successful. The API key is set in the  [docker-compose-hub.yml](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/quick-start/docker-compose-hub.yml?ref_type=heads#L16) file. The curl command is put below or import the [postman collection](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/piveau.postman_collection.json?ref_type=heads)
	```  
	curl --location --request PUT 'http://localhost:8081/catalogues/test' \
	--header 'Content-Type: text/turtle' \
	--header 'X-API-Key: yourRepoApiKey' \
	--data-raw '@prefix dct: <http://purl.org/dc/terms/> .
	@prefix dcat: <http://www.w3.org/ns/dcat#> .

	<https://example.io/id/catalogue/example-catalog>
	    a dcat:Catalog ;
	    dct:type "dcat-ap";
	    dct:title "Example Catalog"@en ;
	    dct:description "This is an example Catalog"@en ;
	    dct:language  <http://publications.europa.eu/resource/authority/language/ENG> ;
	' 
	```
	Go to: http://localhost:8085/shell.html. The CLI of hub-repo and enter: **syncKeycloakGroupsOnce** the catalog in registered into DPI, we can see it in the UI. ![2](/uploads/9c0cfc200255e5089ede17cbab49b380/2.png)
	
- **Create user and group:** we followed the main instructions mentioned [here](https://doc.piveau.eu/guides/integration-keycloak/#create-a-user).

-  **Create Dataset:** Once the catalog is available in the UI, we can use the button called "create dataset" ![3](/uploads/d4ad1556693a6427588cd9b3e2be15da/3.png) to add our own dataset under the catalog. As shown in the picture ![4](/uploads/e90a26a4072c36f308ac5896a0b19e3b/4.png), 
fill out the fields. The mandatory fields are makred as *. We selected the catalog that we created in the previous step.  Once all the fields are filled, we clicked publish dataset at the bottom. And the dataset is available in Piveau. 

![5](/uploads/1dc6dbae7e628182c2dd0e5ef260a5b6/5.png)

- **Edit Dataset:** To edit a dataset, we clicked on the dataset we wanted to edit and then from the bottom panel selected “Edit Dataset”![6](/uploads/e35e88075500fbcf06d4d8df1ca4c13c/6.png).  
Once the fields are filled, again click on publish dataset, and the changes will be  reflected in the UI.

![7](/uploads/ffaa977530e21e5d805c5bcf059d6788/7.png)

- **Delete Dataset:**  Again to delete a dataset, we clicked on the dataset, went to the bottom panel and selected “Delete Dataset”.
![8](/uploads/e6c5b9ea7d90237df722a27912fabf12/8.png)

A video tutorial of the steps can be found [here](https://gitlab.com/TasneemRashid/piveau-fhg/-/blob/master/video/Piveau.mp4).
