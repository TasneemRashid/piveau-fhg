# Change log of the Piveau

## [2024-06-17]

### Added
- Keycloak adaptation: Our realm file contains the following changes from the original files:
	- In the [manual setup](https://doc.piveau.eu/guides/integration-keycloak/#manual-configuration) of the realm, the **rsa-enc-generated** is being deleted. It was not deleted from the original realm file. We did that maually
	- Adapted recommended [CORS settings](https://doc.piveau.eu/guides/integration-keycloak/#cors-settings). 
- Body of API call: added "dcat" prefix in the [provided body](https://doc.piveau.eu/guides/use-the-hub-apis/#create-a-catalogue)
- Adapted docker-compose-hub.yml file 

## [2024-07-04]  

### Updated

- Updated the docker-compose-hub file to solve the issue with editing a dataset via DPI.